replacekey - replace key expressions by templates

Copyright (C) 2016-2017 Francesco Montanari

Licensed under the GNU General Public License version 3 (or later).


Description
-----------

Parse recursively files under a directory (or a single file). Scan
only files with a given extension (e.g. `.ext`). Replace key
expressions in the form `{{key_label}}` by the content set in
respective files under a `./keys/` directory. Templates under this
directory must be in the form `key_label.ext`. A default template is
available under `replacekey/keys/`, and customized keys directory can
be specified by the user. The processed input files are saved under
user-defined output paths.


Install
-------
Scripts are provided to install `replacekey` locally:
```shell
$ bash ./install.sh
```
and to uninstall it
```shell
$ bash ./uninstall.sh
```

Alternatively, for a system-wide installation run (an uninstall script
is not provided in this case):
```shell
# python setup.py install
```
or
```shell
# python3 setup.py install
```


Usage
-----
```
usage: replacekey.py [-h] [--version] [-v] [-r] [-k dir] [-e extension]
                     path_in path_out

Replace key expressions in a file by templates defined under a 'keys/'
directory.

positional arguments:
  path_in               existing file (or folder, if the -r flag is set)
                        containing keys expressions to be replaced
  path_out              file (or folder, if the -r flag is set) where to save
                        the output of the replacements

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -v, --verbose         increase output verbosity
  -r, --recursive       parse and replace keys recursively
  -k dir, --keys-dir dir
                        path to directory containing keys files (default:
                        installation_dir/keys/); key files must have the same
                        extension as the files
  -e extension, --extension extension
                        files extension (default: html)
```


Development
-----------
Checkout the repository. Run the code as [1]:
```shell
python -m replacekey.replacekey *args*
```

Unit tests are provided under `tests`.


[1]: Running `python replacekey/replacekey.py` will rise `ValueError:
     Attempted relative import in non-package`.
