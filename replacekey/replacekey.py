"""Replace key expressions by templates.

   Copyright (C) 2016-2017 Francesco Montanari.

   This file is part of replacekey.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import os
import re
import argparse
import warnings


class ReplaceKey:
    """
    Parse recursively files under a directory, or a single file.  Scan
    files of a given extension and containing specific key expressions
    in the form `{{key_label}}`.  Replace these keys by the content
    set in respective files under a `keys/` directory.  Templates
    under this directory must be in the form `key_label.extension`.
    The default keys and extension can be changed in the `__init__`
    below.

    Parameters
    ----------
    recursive : bool
        If True parse and replace the files recursively. Set to
        False otherwise.
    pathin : str
        Existing directory of files, or file containing the keys
        to be replaced.
    pathout : str
        Output directory, or file. The keys are replaced by the
        content set in the corresponding files under the `keys/`
        directory.  If the output (sub)directory does not exists,
        it will be created.
    dir_keys : list or None
        Optional. Path to directory containing key files. If `None`,
        use default directory.
    extension : str or None
        Optional. Extension of the files to parse. Assume that the
        keys files have the same extension. If `None`, use default
        extension `html`.

    Returns
    -------
     :
        Each file with the given extension under the `pathin` directory is
        saved under the `pathout`. The keys are replaced by the respective
        content.
    """


    def __init__(self,
                 recursive,
                 pathin,
                 pathout,
                 dir_keys=None,
                 extension=None,
                 verbose=False):
        """Initialize and coordinate the execution of the class methods."""
        self.recursive = recursive
        self.pathin = pathin
        self.pathout = pathout
        self.verbose = verbose

        if extension:
            self.extension = extension
        else:
            self.extension = 'html'

        if dir_keys:
            self.pathkey = dir_keys
        else:
            self._setPathkey('keys/')

        if not os.path.isdir(self.pathkey):
            msg = ("%s directory not found" % self.pathkey)
            raise IOError(msg)

        self._setKeys() # Define self.keys

        if os.path.isdir(self.pathin):
            self.saveKeyDir()
        else:
            self.saveKeyFile()


    def _setPathkey(self, dir_keys):
        """Set the keys directory path to the one provided under the
        installed module
        """
        try:
            import replacekey
            module_dir = os.path.dirname(replacekey.__file__)
            self.pathkey = os.path.join(module_dir, dir_keys)

        except ImportError:
            msg = ("replacekey module not installed. Assuming that "
                   "replacekey.py is executed as __main__.")
            warnings.warn(msg, ImportWarning)

            this_dir, _ = os.path.split(__file__)
            self.pathkey = os.path.join(this_dir, dir_keys)


    def _setKeys(self):
        """Keys labels are automatically set according to the file
        names under the keys directory.
        """
        self.keys = []
        for _root, _dirs, filenames in os.walk(self.pathkey, topdown=True):
            for filename in filenames:
                if filename.endswith(self.extension):
                    key, _ext = os.path.splitext(filename)
                    self.keys += ['{{'+key+'}}']
            break # Do not want recursive search here
        if len(self.keys) == 0:
            warnings.warn("No key file found", RuntimeWarning)


    def saveKeyFile(self):
        """Initialize routine to replace keys in file pathin and save
        the output to file pathout.
        """
        if self.verbose:
            print("The keys:\n%s\nhave been replaced and saved in the "
                  "following file:" % str(self.keys))
        self.replaceKey(self.pathin, self.pathout)
        if self.verbose:
            print("%s\n"%self.pathout)


    def saveKeyDir(self):
        """Initialize routine to replace keys in files under the
        pathin directory, and save them under the pathout directory.
        """
        if self.verbose:
            print("The keys:\n%s\nhave been replaced and saved in the "
                  "following files:"%str(self.keys))

        # Here the paths must be directories.  Windows may be probably
        # dealt with '\'. However, this should be
        # experimented. Instead of guessing, just do nothing and
        # assume that the npn-posix users will pass the correct path
        # name.
        if os.name == 'posix':
            if self.pathin != '/':
                self.pathin += '/'
            if self.pathout != '/':
                self.pathout += '/'

        for root, _dirs, filenames in os.walk(self.pathin, topdown=True):
            for filename in filenames:
                if filename.endswith(self.extension):
                    fin = os.path.join(root, filename)
                    rootout = root.replace(self.pathin, self.pathout)
                    if not os.path.exists(rootout):
                        os.makedirs(rootout)
                    fout = os.path.join(rootout, filename)
                    self.replaceKey(fin, fout)
                    if self.verbose:
                        print("%s\n"%fout)
            if not self.recursive:
                break


    def replaceKey(self, fin, fout):
        """Replace keys in file fin and save the output to file fout."""

        extension = '.'+self.extension

        f_in = open(fin, 'r')
        str_in = f_in.read()

        try:
            f_out = open(fout, 'w+')
        except IOError:
            msg = ('Please check that path_in and path_out are both '
                   'files or directories. If path_out is a file, make '
                   'sure that its directory exists.')
            raise IOError(msg)

        rep = {}
        for key in self.keys:
            fkey = key
            for cht in ['{', '}']:
                fkey = fkey.replace(cht, '')
            fkey = os.path.join(self.pathkey, fkey+extension)
            try:
                f_key = open(fkey, "r")
                str_key = f_key.read()
                rep[key] = str_key
                f_key.close()
            except IOError:
                msg = ("Could not open a 'keys/' template corresponding "
                       "to the key %s" % (key))
                raise IOError(msg)

        # Perform the replacement
        rep = dict((re.escape(k), v) for k, v in rep.items())
        pattern = re.compile("|".join(rep.keys()))
        text = pattern.sub(lambda m: rep[re.escape(m.group(0))], str_in)

        f_out.write(text)
        f_out.close()
        f_in.close()


def getParser():
    """Set up and return the parser."""
    parser = argparse.ArgumentParser(
        description=("Replace key expressions in a file by templates "
                     "defined under a 'keys/' directory."),
        epilog=("For more information about the keys template format, "
                "see the documentation of the ReplaceKey() class.")
    )


    from . import __version__
    parser.add_argument('--version', action='version',
                        version='%(prog)s '+__version__)

    parser.add_argument('-v', '--verbose', action="store_true",
                        help="increase output verbosity")

    parser.add_argument('path_in', metavar=('path_in'),
                        type=str, nargs=1,
                        help=('existing file (or folder, if the -r flag '
                              'is set) containing keys expressions to '
                              'be replaced'))

    parser.add_argument('path_out', metavar=('path_out'),
                        type=str, nargs=1,
                        help=('file (or folder, if the -r flag is set) '
                              'where to save the output of the '
                              'replacements'))

    parser.add_argument('-r', '--recursive', action='store_true',
                        help=('parse and replace keys recursively'))

    parser.add_argument('-k', '--keys-dir',
                        metavar=('dir'),
                        type=str,
                        nargs=1,
                        help=('path to directory containing keys files '
                              '(default: installation_dir/keys/); '
                              'key files must have the same extension '
                              'as the files'))

    parser.add_argument('-e', '--extension',
                        metavar=('extension'),
                        type=str,
                        nargs=1,
                        help=('files extension (default: html)'))

    return parser


def callReplaceKey(args):
    """Interpret args and call :class:`ReplaceKey`."""

    pathin = args.path_in[0]
    pathout = args.path_out[0]

    if args.keys_dir:
        keys_dir = args.keys_dir[0]
    else:
        keys_dir = None

    if args.extension:
        extension = args.extension[0]
    else:
        extension = None

    ReplaceKey(args.recursive,
               pathin,
               pathout,
               keys_dir,
               extension,
               args.verbose)


def getCli():
    """Command line interface."""
    parser = getParser()
    args = parser.parse_args()
    callReplaceKey(args)


if __name__ == '__main__':
    getCli()
