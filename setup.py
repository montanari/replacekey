#!/usr/bin/env python

from setuptools import setup, find_packages
import replacekey

setup(name='replacekey',
      version=replacekey.__version__,
      description='Replace key expression by template',
      classifiers=[
          "Development Status :: 4 - Beta",
          "Environment :: Console",
          "Intended Audience :: Developers",
          "Programming Language :: Python :: 2.7",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.2",
          "Programming Language :: Python :: 3.3",
          "Programming Language :: Python :: 3.4",
          "Programming Language :: Python :: 3.5",
      ],
      author='Francesco Montanari',
      author_email='francesco.montanari@openmailbox.org',
      url='https://gitlab.com/montanari/replacekey',
      license='GPL3+',
      packages=find_packages(),
      package_data={'replacekey': ['keys/*.html']},
      entry_points={'console_scripts':
                    ['replacekey = replacekey.replacekey:getCli',]},
)
